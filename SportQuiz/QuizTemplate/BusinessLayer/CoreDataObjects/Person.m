//
//  Person.m
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 11/12/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "Person.h"


@implementation Person

@dynamic boughtPoints;
@dynamic earnedPoints;
@dynamic points;
@dynamic questionIndex1;
@dynamic questionIndex2;
@dynamic questionIndex3;
@dynamic tapjoyedPoints;
@dynamic botWins;
@dynamic botDraws;
@dynamic botLoses;

-(NSInteger)numberOfMatches
{
    return [self.botLoses integerValue] + [self.botWins integerValue] + [self.botDraws integerValue];
}

-(NSNumber*)allPersonPoints
{
    NSInteger value = [self.earnedPoints integerValue] + [self.boughtPoints integerValue] + [self.tapjoyedPoints integerValue];
    return [NSNumber numberWithInteger:value];
}


@end
