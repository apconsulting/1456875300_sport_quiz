//
//  PersonInitializer.m
//  AudioQuiz
//
//  Created by Vladislav on 4/8/13.
//  Copyright (c) 2013 Vladislav. All rights reserved.
//

#import "PersonInitializer.h"
#import "Person.h"

@implementation PersonInitializer

+(BOOL)initializePersonWithEarnedCoins:(NSInteger)coins boungtCoins:(NSInteger)bought questionIndex1:(NSInteger)qIndex1
                        questionIndex2:(NSInteger)qIndex2 questionIndex3:(NSInteger)qIndex3;
{
    
    Person *person = (Person*)[Person createObject];
    
    person.earnedPoints = [NSNumber numberWithInteger:coins];
    person.boughtPoints = [NSNumber numberWithInteger:bought];
    person.tapjoyedPoints = [NSNumber numberWithInteger:0];
    person.questionIndex1 = [NSNumber numberWithInteger:qIndex1];
    person.questionIndex2 = [NSNumber numberWithInteger:qIndex2];
    person.questionIndex3 = [NSNumber numberWithInteger:qIndex3];
    person.points = [NSNumber numberWithInteger:0];
    person.botDraws = [NSNumber numberWithInteger:0];
    person.botLoses = [NSNumber numberWithInteger:0];
    person.botWins = [NSNumber numberWithInteger:0];
    return YES;
}

@end
