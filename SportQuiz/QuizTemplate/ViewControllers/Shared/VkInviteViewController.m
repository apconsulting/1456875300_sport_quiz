//
//  VkInviteViewController.m
//  CarQuiz
//
//  Created by zlata samarskaya on 21.10.15.
//  Copyright © 2015 Uladzislau Yasnitski. All rights reserved.
//

#import "VkInviteViewController.h"

#import "VKSdk.h"
#import "VKApiFriends.h"
#import "VKUser.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "GameModel.h"
#import "ConstantsAndMacros.h"
#import "Flurry.h"
@import FirebaseAnalytics;

@interface VkInviteViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property(nonatomic, strong)UITableView *tableView;
@property(nonatomic, strong)NSArray *friends;
@property(nonatomic, weak)VKUser *currentFriend;
@property(nonatomic, weak)VKGroup *currentGroup;

@end

@implementation VkInviteViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(vkDidAuthorized) name:@"vkDidAuthorized" object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];

    if (![VKSdk isLoggedIn]) {
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        [delegate authorizeVK];
    } else {
        [self loadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)loadData {
    [self loadFriends];
   // [self loadGroup];
}

- (void)loadFriends {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        VKRequest *request = [[VKApi friends] get:@{VK_API_FIELDS:@[@"first_name", @"last_name", @"photo_50", @"status"]}];
    [request executeWithResultBlock:^(VKResponse * response) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        self.friends = response.parsedModel;
        [self.tableView reloadData];
    } errorBlock:^(NSError * error) {
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [[[UIAlertView alloc] initWithTitle:@"Ошибка"
                                        message:@"Не удалось загрузить список друзей"
                                       delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
            [self.navigationController popViewControllerAnimated:YES];
            NSLog(@"VK error: %@", error);
        } 
    }];
}

- (void)loadGroup {
    VKRequest *request = [[VKApi groups] getById:@{VK_API_GROUP_ID : @"iosgamesru"}];
    [request executeWithResultBlock:^(VKResponse * response) {
        self.currentGroup = response.parsedModel;
    } errorBlock:^(NSError * error) {
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
        } else {
            NSLog(@"VK error: %@", error);
        }
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.friends.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    VKUser *friend = self.friends[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:@"vkDefault"];
    cell.imageView.layer.cornerRadius = 25;
    cell.imageView.clipsToBounds = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSString* imgPath = friend.photo_50;
        NSData* img = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgPath]];

        dispatch_async(dispatch_get_main_queue(), ^{
            cell.imageView.image = [UIImage imageWithData:img];
        });
    });

    NSString* firstName = friend.first_name;
    NSString* lastName = friend.last_name;
    NSString* fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    cell.textLabel.text = fullName;

    NSString* status = friend.status;
    cell.detailTextLabel.text = status;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.currentFriend = self.friends[indexPath.row];
    NSString* fullName = [NSString stringWithFormat:@"%@ %@", self.currentFriend.first_name, self.currentFriend.last_name];
   [[[UIAlertView alloc] initWithTitle:@"Пригласить друга"
                               message:[NSString stringWithFormat:@"Пригласить %@ в Угадай авто?", fullName] delegate:self cancelButtonTitle:@"Отмена" otherButtonTitles:@"Пригласить", nil] show];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.cancelButtonIndex) {
        return;
    }
    [self sendInvite];
}

- (void)sendInvite {
    VKRequest *request = [VKRequest requestWithMethod:@"apps.sendRequest"
//                                        andParameters:@{@"text" : [NSString stringWithFormat:@"Присоединяйся к Угадай авто! Отличная викторина!"], VK_API_USER_ID : self.currentFriend.id, @"type": @"invite"}
//                                        andHttpMethod:@"POST"];
    parameters:@{@"user_id" : self.currentFriend.id, @"text" : @"Присоединяйся к Угадай авто! Отличная викторина!", @"type" : @"invite"}];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    VKRequest *postReq = [[VKApi wall] post:@{VK_API_MESSAGE : [NSString stringWithFormat:@"Присоединяйся к %@ %@", shareLinkTextToVK, urlAppForVK], VK_API_OWNER_ID : self.currentFriend.id}];
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    [postReq executeWithResultBlock:^(VKResponse * response) {
    [request executeWithResultBlock:^(VKResponse * response) {
   // [postReq executeWithResultBlock:^(VKResponse * response) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [Flurry logEvent:@"VK - Пригласил друга"];
        [FIRAnalytics logEventWithName:@"Invite_friend" parameters:nil];
        [[GameModel sharedInstance] earnCoins:vkInviteFriendErnedCoins];
        [[[UIAlertView alloc] initWithTitle:@"Приглашение выслано"
                                    message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
        [self.navigationController popViewControllerAnimated:YES];
    } errorBlock:^(NSError * error) {
       if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [[[UIAlertView alloc] initWithTitle:@"Ошибка"
                                        message:[NSString stringWithFormat:@"Не удалось выслать приглашение\n%@", [error localizedDescription]]
                                       delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
            NSLog(@"VK error: %@", error);
        }
    }];   //}
    //if (self.currentGroup) {
}

- (void)vkDidAuthorized {
    if (![VKSdk isLoggedIn]) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    [self loadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
