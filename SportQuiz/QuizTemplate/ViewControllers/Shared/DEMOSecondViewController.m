//
//  DEMOSecondViewController.m
//  REFrostedViewControllerStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOSecondViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "Person.h"
#import "Flurry.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface DEMOSecondViewController ()<UIAlertViewDelegate>

@end

@implementation DEMOSecondViewController

-(void)viewDidLoad{
    [super viewDidLoad];

    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SubsribeView" owner:self options:nil];
    UIView *mainView = [subviewArray objectAtIndex:0];
    mainView.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2-60);
    [self.view addSubview:mainView];
    
    self.txtEmail = (UITextField*)[self.view viewWithTag:1];
    self.txtName = (UITextField*)[self.view viewWithTag:2];
    
    [((UIButton*)[self.view viewWithTag:3]) addTarget:self action:@selector(click_btn_subscribe:) forControlEvents:UIControlEventTouchUpInside];
    [((UIButton*)[self.view viewWithTag:4]) addTarget:self action:@selector(click_btn_exit:) forControlEvents:UIControlEventTouchUpInside];
    [((UIButton*)[self.view viewWithTag:44]) addTarget:self action:@selector(click_btn_exit:) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)keyboardDidShow: (NSNotification *) notif{
    if(!IS_IPHONE_5 && !isPad)
        [UIView animateWithDuration:.5 animations:^{
            self.view.frame = CGRectOffset(self.view.frame, 0, -100);
        }];
}

- (void)keyboardDidHide: (NSNotification *) notif{
    if(!IS_IPHONE_5 && !isPad)
        [UIView animateWithDuration:.5 animations:^{
            self.view.frame = CGRectOffset(self.view.frame, 0, 100);
        }];

}

- (IBAction)click_btn_exit:(id)sender {
    [Flurry logEvent:@"Отказался от подписки"];

    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)click_btn_subscribe:(id)sender {
    [Flurry logEvent:@"Нажал подписаться"];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.view endEditing:YES];
    
    [self.connection cancel];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:@"https://smartresponder.ru/subscribe.html"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    [request setHTTPMethod:@"POST"];
    NSString *postData = [NSString stringWithFormat:@"field_email=%@&field_name_first=%@&uid=321070&did=383084&tid=966908",self.txtEmail.text,self.txtName.text];
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    [connection start];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"%@" , error);
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                    message:@"Не удалось установить соединение, попробуйте позже"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              encoding:NSUTF8StringEncoding];
    
    if([htmlSTR rangeOfString:@"appcompass.ru"].location != NSNotFound && htmlSTR){
        
        [Flurry logEvent:@"правильно ввел все данные в подписке"];
        
        Person *p = [[Person allObjects] lastObject];
        p.boughtPoints = [NSNumber numberWithInteger:[p.boughtPoints integerValue] + 100];
        [DELEGATE saveContext];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isSubscribe"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Подтвердите подписку на почте и получите монетки!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                        message:@"Проверте правильность введеных данных!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    [(UINavigationController*)self.presentingViewController popViewControllerAnimated:NO];
    [(UINavigationController*)self.presentingViewController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
