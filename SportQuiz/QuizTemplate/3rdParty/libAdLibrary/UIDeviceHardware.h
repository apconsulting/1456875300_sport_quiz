//
//  UIDeviceHardware.h
//  LivingRooms
//
//  Created by Vladislav on 4/24/13.
//  Copyright (c) 2013 Vladislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIDeviceHardware : NSObject
- (NSString *) platform;
- (NSString *) platformString;
- (NSUInteger) platformIndex;
@end
