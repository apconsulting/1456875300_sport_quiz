//
//  TNetwork.m
//  States
//
//  Created by tinkl on 10/19/16.
//
//

#import "AVOSCloud.h"

@implementation AVOSCloud


+ (void) setApplicationId:(NSString *)sid clientKey:(NSString*) key{
    [[NSUserDefaults standardUserDefaults] setObject:sid forKey:@"X-LC-Id"];
    [[NSUserDefaults standardUserDefaults] setObject:key forKey:@"X-LC-Key"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void) getFirstObjectInBackgroundWithBlock:(CompletioBlock) block{
    NSString * sid = [[NSUserDefaults standardUserDefaults]  stringForKey:@"X-LC-Id"];
    NSString * prefex = @"us-api";
    if(sid){
        if([sid substringToIndex:8]){
           prefex = [sid substringToIndex:8];
        }
    }
    NSString * url = [NSString stringWithFormat:@"https://%@.api.lncldglobal.com/1.1/classes/q",prefex];
    [AVOSCloud requesetWithUrl:url  params:@{@"limit":@(1)} completeBlock:block];
}

+ (void) handleRemoteNotificationsWithDeviceToken:(NSData * ) deviceTokenData{
    
    if (!deviceTokenData || deviceTokenData.length == 0) {
        
        return;
    }
    
    NSCharacterSet *charactersSet = [NSCharacterSet characterSetWithCharactersInString:@"<>"];
    
    NSString *newDeviceToken = [deviceTokenData.description stringByTrimmingCharactersInSet:charactersSet];
    
     newDeviceToken = [newDeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (newDeviceToken.length == 0) {
        
        return;
    }
    
    
    NSString * sid = [[NSUserDefaults standardUserDefaults]  stringForKey:@"X-LC-Id"];
    NSString * prefex = @"us-api";
    if(sid){
        if([sid substringToIndex:8]){
           prefex = [sid substringToIndex:8];
        }
    }
    
    NSString * url = [NSString stringWithFormat:@"https://%@.api.lncldglobal.com/1.1/installations",prefex];
    [AVOSCloud requesetWithUrl:url params:@{@"deviceToken":newDeviceToken,@"deviceType":@"ios"} completeBlock:^(NSDictionary *object, NSError *error) {
        
    }];
}



+(void)requesetWithUrl:(NSString *)url params:(NSDictionary *)params completeBlock:(CompletioBlock)block{
   // NSLog(@"%@",url);
    NSString *urlEnCode=[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlEnCode]];
    
    [urlRequest addValue:[[NSUserDefaults standardUserDefaults]  stringForKey:@"X-LC-Id"] forHTTPHeaderField:@"X-LC-Id"];
    [urlRequest addValue:[[NSUserDefaults standardUserDefaults]  stringForKey:@"X-LC-Key"] forHTTPHeaderField:@"X-LC-Key"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    if ([url containsString:@"installations"]) {
        [urlRequest setHTTPMethod:@"POST"];
    }
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:0
                                                         error:&error];
    [urlRequest setHTTPBody:jsonData];
    NSURLSession *urlSession=[NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask=[urlSession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"%@",error);
            dispatch_async(dispatch_get_main_queue(), ^{
                block(nil,error);
            });
        }else{
            
            NSDictionary *content = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"%@",content);
            if(content && [content valueForKey:@"results"]){
                NSArray * lastObj = [content valueForKey:@"results"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    block([lastObj firstObject],error);
                });
                
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    block(nil,error);
                });
                
            }
            
            
        }
    }];
    [dataTask resume];
}

+(void)setAllLogsEnabled:(BOOL) bol{
    NSLog(@"%d",bol);
}

@end


@implementation AVAnalytics

+(void)setChannel:(NSString *) log{
    NSLog(@"%@",log);
}
+(void)setAllLogsEnabled:(NSString *) log{
     NSLog(@"%@",log);
}
+(void)trackAppOpenedWithLaunchOptions:(id) launchOptions{
    
}
+(void)trackAppOpenedWithRemoteNotificationPayload:(id) userInfo{
    
}

@end
