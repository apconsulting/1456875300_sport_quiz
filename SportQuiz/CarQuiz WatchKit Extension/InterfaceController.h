//
//  InterfaceController.h
//  CarQuiz WatchKit Extension
//
//  Created by zlata samarskaya on 10.05.15.
//  Copyright (c) 2015 Uladzislau Yasnitski. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface InterfaceController : WKInterfaceController

@end
