//
//  InterfaceController.m
//  CarQuiz WatchKit Extension
//
//  Created by zlata samarskaya on 10.05.15.
//  Copyright (c) 2015 Uladzislau Yasnitski. All rights reserved.
//

#import "InterfaceController.h"
#import "QuestionNew.h"
#import "QustionInfo.h"
#import "UIColor+NewColor.h"
#import "NSMutableArray+Shuffling.h"

#import <WatchConnectivity/WatchConnectivity.h>

#import "FlurryWatch.h"

@interface InterfaceController() {

NSDictionary *_answers;
}

@property(nonatomic, weak)IBOutlet WKInterfaceImage *imageView;
@property(nonatomic, weak)IBOutlet WKInterfaceLabel *pointsLabel;
@property(nonatomic, weak)IBOutlet WKInterfaceLabel *countLabel;
@property(nonatomic, weak)IBOutlet WKInterfaceButton *firstButton;
@property(nonatomic, weak)IBOutlet WKInterfaceButton *secondButton;
@property(nonatomic, weak)IBOutlet WKInterfaceButton *thirdButton;
@property(nonatomic, weak)IBOutlet WKInterfaceButton *forthButton;
@property(nonatomic, strong)NSDictionary *question;

- (IBAction)didAnswer:(id)sender;
- (IBAction)didAnswerSec:(id)sender;
- (IBAction)didAnswerThird:(id)sender;
- (IBAction)didAnswerForth:(id)sender;

@end

@implementation InterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    // Animate a series of images prefixed with the string "Activity"
    [self.imageView setImageNamed:@"Activity"];
    [self.imageView startAnimatingWithImagesInRange:NSMakeRange(0, 15)
                                                duration:1.0
                                             repeatCount:0];
      [self startGame];
}

- (void)startGame {
    
//    [InterfaceController openParentApplication:@{@"request":@"startOffline"}
//                                         reply:^(NSDictionary *replyInfo, NSError *error) {
//                                             if (!error) {
//                                                 [FlurryWatch logWatchEvent:@"Start new game on Watch"];
//                                                 [self loadQuestion];
//                                             } else {
//                                                 NSLog(@"start error %@", error);
//                                             }
//                                         }];
    
    [[WCSession defaultSession] sendMessage:@{@"request":@"startOffline"}
                               replyHandler:^(NSDictionary *reply) {
                                   //handle reply from iPhone app here
                                   [FlurryWatch logWatchEvent:@"Start new game on Watch"];
                                   [self loadQuestion];
                               }
                               errorHandler:^(NSError *error) {
                                   //catch any errors here
                                   NSLog(@"start error %@", error);
                               }
     ];

}

- (BOOL)didAnswerQuestion:(NSInteger)index {
    if (!_question) {
        return NO;
    }
    NSDictionary *answer = [_question[@"answers"] objectAtIndex:index];
    if ([answer[@"bIsRightAnswer"] integerValue]) {
//        [InterfaceController openParentApplication:@{@"request":@"didRightAnswer"}
//                                             reply:^(NSDictionary *replyInfo, NSError *error) {
//                                                 if (!error) {
//                                                     _question = replyInfo[@"question"];
//                                                     [self updateQuestion];
//                                                     [self updatePoints:replyInfo];
//                                                 } else {
//                                                     NSLog(@"start error %@", error);
//                                                 }
//                                             }];
        [[WCSession defaultSession] sendMessage:@{@"request":@"didRightAnswer"}
                                   replyHandler:^(NSDictionary *reply) {
                                       //handle reply from iPhone app here
                                       _question = reply[@"question"];
                                       [self updateQuestion];
                                       [self updatePoints:reply];
                                   }
                                   errorHandler:^(NSError *error) {
                                       //catch any errors here
                                       NSLog(@"start error %@", error);
                                   }
         ];
        return YES;
    }
    
//    [InterfaceController openParentApplication:@{@"request":@"didWrongAnswer"}
//                                         reply:^(NSDictionary *replyInfo, NSError *error) {
//                                             if (!error) {
//                                                 _question = replyInfo[@"question"];
//                                                 [self updateQuestion];
//                                                 [self updatePoints:replyInfo];
//                                             } else {
//                                                 NSLog(@"start error %@", error);
//                                             }
//                                         }];
    
    [[WCSession defaultSession] sendMessage:@{@"request":@"didWrongAnswer"}
                               replyHandler:^(NSDictionary *reply) {
                                   //handle reply from iPhone app here
                                   _question = reply[@"question"];
                                   [self updateQuestion];
                                   [self updatePoints:reply];
                               }
                               errorHandler:^(NSError *error) {
                                   //catch any errors here
                                   NSLog(@"start error %@", error);
                               }
     ];

    
    return NO;

}

- (void)updatePoints:(NSDictionary*)replyInfo {
    NSNumber *p = replyInfo[@"points"];
    NSString *count = replyInfo[@"count"];
    [_pointsLabel setText:[NSString stringWithFormat:@"%i", [p integerValue]]];
    [_countLabel setText:[NSString stringWithFormat:@"%@", count]];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

- (void)colorButton: (WKInterfaceButton*)sender {
    [sender setBackgroundColor:[UIColor blackColor]];
}

- (void)didAnswer:(BOOL)right button:(WKInterfaceButton*)sender {
    if (!right) {
        [sender setBackgroundColor:[UIColor redColor]];
        [self performSelector:@selector(colorButton:) withObject:sender afterDelay:1.3];
    }
}

- (void)didAnswer:(WKInterfaceButton*)sender {
    [self didAnswer:[self didAnswerQuestion:0] button:sender];
}

- (void)didAnswerSec:(id)sender {
    [self didAnswer:[self didAnswerQuestion:1] button:sender];
}

- (void)didAnswerThird:(id)sender {
    [self didAnswer:[self didAnswerQuestion:2] button:sender];
}

- (void)didAnswerForth:(id)sender {
    [self didAnswer:[self didAnswerQuestion:3] button:sender];
}

- (void)loadQuestion {
//    [InterfaceController openParentApplication:@{@"request":@"question"}
//                                           reply:^(NSDictionary *replyInfo, NSError *error) {
//                                               
//                                               // the request was successful
//                                               if(error == nil) {
//                                                   if (!replyInfo) {
//                                                       return;
//                                                   }
//                                                   
//                                                   // get the serialized location object
//                                                   _question = replyInfo[@"question"];
//                                                   [self updateQuestion];
//                                                   [self updatePoints:replyInfo];
//
//                                               } else {
//                                                   NSLog(@"error %@", error);
//                                               }
//                                           }];
    
    [[WCSession defaultSession] sendMessage:@{@"request":@"question"}
                               replyHandler:^(NSDictionary *reply) {
                                   if (!reply) {
                                       return;
                                   }
                                   _question = reply[@"question"];
                                   [self updateQuestion];
                                   [self updatePoints:reply];
                               }
                               errorHandler:^(NSError *error) {
                                   //catch any errors here
                                   NSLog(@"start error %@", error);
                               }
     ];

}

- (void)updateQuestion {
    [_imageView setImageData:_question[@"questionImage"]];
    [_imageView setWidth:154];
    [_imageView setHeight:110];
    
    _answers = [NSDictionary dictionaryWithObjects:_question[@"answers"] forKeys:[NSArray arrayWithObjects:[NSNumber numberWithInt:0],[NSNumber numberWithInt:1],[NSNumber numberWithInt:2],[NSNumber numberWithInt:3], nil]];
    //                                                   NSMutableArray *colors = [[NSMutableArray alloc] initWithObjects:[UIColor colorWith8BitRed:115 green:121 blue:153 alpha:1],[UIColor colorWith8BitRed:219 green:87 blue:49 alpha:1], [UIColor colorWith8BitRed:0 green:166 blue:165 alpha:1], [UIColor colorWith8BitRed:6 green:179 blue:87 alpha:1], nil];
    //                                                   [colors shuffle];
    
    NSArray *buttons = @[_firstButton, _secondButton, _thirdButton, _forthButton];
    [_answers enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, id obj, BOOL *stop) {
        WKInterfaceButton *btn = [buttons objectAtIndex:[key integerValue]];
        NSDictionary *answer = [_answers objectForKey:key];
        
        [btn setTitle:answer[@"title"]];
        //                                                       [btn setBackgroundColor:[colors objectAtIndex:[key integerValue]]];
    }];
    
    // the request failed

}

@end



